import React from "react"
import {  Routes, Route,BrowserRouter as Router } from 'react-router-dom';
import {  Layout ,HomePage,Login,Register,NotFound } from './components';

import './App.css';

function App() {
  return (
    <Router>
      <Layout>
          <Routes>
            <Route exact path='/' element={<HomePage/>} />
            <Route path='/Login' element={<Login/>} />
            <Route path='/Register' element={<Register/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
      </Layout>
    </Router>
  );
}

export default App;
