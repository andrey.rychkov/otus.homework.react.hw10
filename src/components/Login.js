import {  Button,Form,Card} from 'react-bootstrap';

export default function Login() {
    const pageName ="Login Page";
    return (
        <div>
            <Card style={{ top: '30px',width:'500px' }}>
                <Card.Header as="h5">{pageName}</Card.Header>
                <Card.Body>
                    <Card.Title>Special title treatment</Card.Title>
                      <Form>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                            <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        
                        <Button  type="submit">Submit</Button>
                        </Form>
                </Card.Body>
                </Card>
                </div>

    );
}