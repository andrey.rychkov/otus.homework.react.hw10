import Layout from './Layout';
import HomePage from './HomePage';
import Login from './Login';
import Register from './Register';
import NotFound from './NotFound';

export { HomePage, Login,Register,NotFound ,Layout};