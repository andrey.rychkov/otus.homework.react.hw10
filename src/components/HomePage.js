import { useSelector, useDispatch } from 'react-redux'
import { Button, Card,Badge} from 'react-bootstrap';
import { decrement, increment } from './counterSlice'

export default function HomePage() {

    const pageName ="HomePage";
    const count = useSelector((state) => state.counter.value)
    const dispatch = useDispatch()

    return (
        <Card style={{ top: '30px',width:'500px' }}>
            <Card.Header as="h5">{pageName}</Card.Header>
            <Card.Body>
                        <Button  onClick={() => dispatch(increment())}  type="submit">+</Button>
                        <Badge pill bg="success">{count}</Badge>
                        <Button  onClick={() => dispatch(decrement())}  type="submit">-</Button>
            </Card.Body>
        </Card>
    );
}