import React, { Component } from 'react';
import { Nav,Container, Navbar} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
import './NavMenu.css';

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <Navbar bg="light" expand="lg">
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <LinkContainer to="/"><Nav.Link>Homepage</Nav.Link></LinkContainer>
              <LinkContainer to="/Login"><Nav.Link>Login</Nav.Link></LinkContainer>
              <LinkContainer to="/Register"><Nav.Link>Register</Nav.Link></LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
                  
    );
  }
}
